# Test Savoir dessiner
## Candidature Progressif Média.

##This project requires:
- Apache
- PHP 7+
- Node: 12+

## Setup projet

setup `npm install` 

for dev `gulp`

for prod `gulp production`

## Gulp command

## *Command overview :*

`gulp`

*Setup by NODE-ENV*
Move /app files to /dist (html, php, js, fonts)
Sass compile with sourcemap
image optimization and move to /dist
watch files changed
browser live reload

`gulp production`

clean all (except img)
Move /app files to /dist (html, js, fonts)
image optimization and move to /dist
Sass compile without sourcemap
CleanCSS + minify + autoprefixer (> 0.5%, last 2 versions, Firefox ESR, not dead)
Js Compile + minify + strip console.log/comment
Html src changed from uniminify to minify version
Html minify except `<?php ?>` tag

## *Single command :*

`gulp cleanAll` = remove all files in /dist
`gulp devMove` = Move .html, .php, .js, .fonts to /dist folder
`gulp devSass` = Compile unstyled Sass with sourcemap
`gulp images` = Launch batch optimization of whole images and move it from /app to /dist
`gulp cleanProd` = Clean all /dist/* folder except /assets/img
`gulp prodMove` = Move .html, .php, .js, .fonts to /dist folder
`gulp prodSass` = Compile Sass without sourcemap
`gulp cssMinify` = Minify all .css in /dist/assets/css
`gulp jsMinify` = Minify all .js in /dist/assets/js

`gulp jsCompile` = Babel config es6>es5
`gulp useRef` = change src in .html/.php from dev to minify version (see below for use)
`gulp htmlMinify` = Minify all .html/.php in /dist, remove comment, ignore tag `<?php ?>`
*For relaunch the Browser Synchronisation :* `gulp watchFile`

## UseCase of UseRef

In /app folder, place in .html/.php file this comment before and after the src call

```jsx
<!--build:css assets/css/style.min.css -->
    <link rel="stylesheet" href="assets/css/style.css">
<!-- endbuild -->
```

or for js

```jsx
<!--build:js assets/js/main.min.js -->
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/main.js"></script>
<!-- endbuild -->
```

for remove

```jsx
<!--build:remove; -->

<script type='text/javascript'>
</script>

<!-- endbuild -->
```

When you call `gulp useRef` gulp replace the src by the build: mentionned.
In this case, the build: is use for prod, with the final name of sources files

Useref **concatenate and replace** whole file you place in comment

## NOTA: for PHP project

For launch BrowserSync in PHP Projet you must add this following line just after on top `<body>`
In this example, we remove the script for the PROD_ENV

```jsx
<!--build:remove; -->

<script type='text/javascript' id="__bs_script__">
    //<![CDATA[
    document.write("<script async src='/browser-sync/browser-sync-client.js'><\\/script>".replace("HOST", location.hostname));
    //]]>
</script>

<!-- endbuild -->
```

