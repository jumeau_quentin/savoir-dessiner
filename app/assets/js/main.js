// ROOT DOCUMENT

document.querySelector('.main-menu .burger').addEventListener('click', function () {
    document.querySelector('.main-menu').classList.toggle('main-menu--opened')
})

const scrollReveal = () => {
    const revealPoint = 100
    const revealElement = document.querySelectorAll(".scrollreveal")
    for (var i = 0; i < revealElement.length; ++i) {
        var windowHeight = window.innerHeight
        var revealTop = revealElement[i].getBoundingClientRect().top
        if (revealTop < windowHeight - revealPoint) {
            revealElement[i].classList.add("isreveal")
            revealElement[i].classList.remove("scrollreveal")
        } else {
            revealElement[i].classList.remove("isreveal")
            revealElement[i].classList.add("scrollreveal")
        }
    }
}

const scrollMenu = () => {
    const revealElement = document.querySelector(".main-menu")
    const scrollPosition = window.scrollY
    if (scrollPosition >= 100) {
        revealElement.classList.add("isscrolled")
        // revealElement[i].classList.remove("isscrolled")
    } else if (scrollPosition < 1) {
        revealElement.classList.remove("isscrolled")
    }
}



window.addEventListener("scroll", scrollMenu)
window.addEventListener("scroll", scrollReveal)

scrollReveal()