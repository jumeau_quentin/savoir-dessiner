<?php
function __uri($site, $path)
{
    echo $site . $path;
}

function __site_uri($path)
{
    global $SITE_URI;
    echo $SITE_URI . $path;
}

function __img($img)
{
    global $SITE_URI;
    echo $SITE_URI . 'assets/img/' . $img;
}

function __css($css)
{
    global $SITE_URI;
    echo $SITE_URI . 'assets/css/' . $css;
}

function __script($script)
{
    global $SITE_URI;
    echo $SITE_URI . 'assets/js/' . $script;
}