<?php
$ch = curl_init();

curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // VOLONTAIRE POUR LE TEST
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_URL, 'https://jsonplaceholder.typicode.com/posts');
$result = curl_exec($ch);
curl_close($ch);

$obj = json_decode($result, true);

global $filterpost;
$filterpost = [];

foreach ($obj as $key => $post) {
    // TITRE QUI CONTIENT L'occurrence "LOREM" et non comme un mot.
    if (strpos($post['title'], 'lorem') !== FALSE) {
       array_push($filterpost, $post);
    }
}
