<?php
include_once 'config/env.php';
include_once 'config/functions.php';
include_once 'controller/Articles.php';

$seoTitle       = 'Savoir dessiner';
$seoDescription = 'Test Quentin JUMEAU — Progressif Média';

include 'template-parts/header.php';
?>

<body>
<?php include 'template-parts/menu.php'; ?>
<main>
    <section class="bg-yellow p-y-xl wrapper-brush">
        <div class="container">
            <div class="row">
                <div class="col va-center">
                    <p class="chapo">L'atelier online</p>
                    <h1 class="page-title title-brush brush-white scrollreveal">Apprenez le dessin et la peinture depuis chez vous</h1>
                </div>
                <div class="col va-center">
                    <div class="media-rounded media-video scrollreveal">
                        <video src="" poster="<?php __img('photos/hero-video.jpg'); ?>">
                        </video>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="p-y-xl bg-grey-2 bloc-reassurance">
        <div class="container">
            <div class="row mb-lg">
                <div class="col ta-center bloc-reassurance--title mb-lg">
                    <h2 class="card-title">Votre école d’art en accès libre et illimité.</h2>
                </div>
            </div>
            <div class="row">
                <div class="col ta-center bloc-reassurance--content">
                    <img class="bloc-reassurance--icon icon-brush scrollreveal mb-md delay-2" src="<?php __img('icon/carte.svg'); ?>" width="50">
                    <h3 class="card-title">À la carte</h3>
                    <p class="text darkgray">+ de 150 heures de cours accessibles où vous voulez, quand vous voulez.</p>
                </div>
                <div class="col ta-center bloc-reassurance--content">
                    <img class="bloc-reassurance--icon icon-brush scrollreveal mb-md delay-4" src="<?php __img('icon/play-green.svg'); ?>" width="50">
                    <h3 class="card-title">Accessible à tous</h3>
                    <p class="text darkgray">Débutants comme confirmés, nos cours vous accompagnent pas à pas dans toutes vos envies.</p>
                </div>
                <div class="col ta-center bloc-reassurance--content">
                    <img class="bloc-reassurance--icon icon-brush scrollreveal mb-md delay-6" src="<?php __img('icon/engagement.svg'); ?>" width="50">
                    <h3 class="card-title">Sans engagement</h3>
                    <p class="text darkgray">Vous êtes libres d’annuler votre abonnement à tout moment&nbsp;!</p>
                </div>
            </div>
        </div>
    </section>

    <section class="p-y-xl bg-white bloc-actualites wrapper-brush">
        <div class="container">
            <div class="row">
                <div class="col ta-center mb-xl">
                    <h2 class="title title-brush brush-yellow scrollreveal mb-lg">Découvrez et créez,<br>tout simplement.</h2>
                    <p class="text">Des centaines de cours et live à la difficulté progressive pour maitriser les bases, affirmer sa pratique et développer<br>une approche personnelle du dessin.</p>
                </div>
            </div>
            <div class="row-grid">
                <?php
                if ($filterpost) :
                    $i = 0;
                    foreach ($filterpost as $post) :
                        $title = $post['title'];
                        $id = $post['id'];
                        $text = $post['body'];
                        $thumb = 'photos/thumb.jpg';
                        $i++;
                        ?>
                        <div class="col">
                            <?php include "template-parts/article.php"; ?>
                        </div>
                    <?php endforeach; else : ?>
                    <div class="col ta-center">
                        <p class="text">Pas encore de cours pour le moment...</p>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </section>

    <section class="p-y-xl bg-white bloc-offre">
        <div class="container">
            <div class="row">
                <div class="col bloc-offre--img va-center">
                    <div class="media-rounded scrollreveal">
                        <img src="<?php __img('photos/hero-video.jpg') ?>" alt="Vos sens artistiques">
                    </div>
                </div>
                <div class="col bloc-offre--content va-center">
                    <h2 class="title">Explorez vos sens artistiques</h2>
                    <span class="border-brush scrollreveal"></span>
                    <p class="text mb-lg">Quelque soit votre niveau, découvrez des centaines de cours accessibles partout, à tout moment. Pour vous exercer pas à pas en dessin et en peinture, à votre rythme.</p>
                    <a href="" class="cta-outline-black">Démarrer les cours en ligne</a>
                </div>
            </div>
        </div>
    </section>

    <section class="p-y-xl bg-white bloc-offre">
        <div class="container">
            <div class="row">
                <div class="col bloc-offre--content va-center">
                    <h2 class="title">Découvrez toutes les techniques</h2>
                    <span class="border-brush scrollreveal"></span>
                    <p class="text mb-lg">Laissez vous guider exercice par exercice, conseil par conseil, pour découvrir et pratiquer toutes les disciplines du dessin et de la peinture.</p>
                    <a href="" class="cta-outline-black">Je m’abonne</a>
                </div>
                <div class="col bloc-offre--img va-center">
                    <div class="media-rounded scrollreveal">
                        <img src="<?php __img('photos/offre-2.jpg') ?>" alt="Techniques de dessins et peinture">
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>
<?php
include 'template-parts/footer.php';
include 'template-parts/script.php';
?>
</body>
</html>
