<article id="<?= $id; ?>" class="card bg-grey-1 border-grey-4">
    <div class="card--img media-rounded scrollreveal transition-1-slow delay-<?= $i; ?>">
        <a href="<?= $id; ?>">
            <img src="<?php __img($thumb); ?>" width="200" alt="<?= $title; ?>">
    </div>
    </a>
    <div class="card--content">
        <ul class="card--meta">
            <li class="card--meta-level green">Tous niveaux</li>
            <li class="card--meta-time darkgray">7 H</li>
            <li class="card--meta-number darkgray">11 cours</li>
        </ul>
        <div class="card--content-text">
            <h2 class="card-title black mb-sm"><a href="<?= $id; ?>"><?= $title; ?></a></h2>
            <p class="text darkgray"><?= $text; ?></p>
        </div>
        <div class="card--content-cta">
            <a href="<?= $id; ?>" class="cta-yellow">
                Commencer
            </a>
        </div>
    </div>
</article>