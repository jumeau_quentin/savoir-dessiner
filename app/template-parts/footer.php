<footer class="bg-black p-y-xl footer mt-xl">
    <div class="container">
        <div class="row">
            <div class="col">
                <img src="<?php __img('logo/logo-footer.svg'); ?>" alt="Savoir dessiner, votre école d'art">
                <p class="text white m-y-md">
                    <a href="mailto:info@savoirdessinerparis.fr">info@savoirdessinerparis.fr</a></p>
                <ul class="footer--rs">
                    <li></li>
                    <li></li>
                    <li></li>
                    <li></li>
                </ul>
            </div>
            <div class="col">
                <h2 class="fw-medium fz-medium white brush-before brush-blue scrollreveal">Atelier Commerce</h2>
                <p class="text white">8 rue de l’Abbé Groult, 75015 Paris</p>
                <p class="text white"><a href="tel:0665308859">06 65 30 88 59</a></p>
            </div>
            <div class="col">
                <h2 class="fw-medium fz-medium white brush-before brush-red scrollreveal">Atelier Vaugirard</h2>
                <p class="text white">2 rue Petel, 75015 Paris</p>
                <p class="text white"><a href="tel:0665308859">06 65 30 88 59</a></p>
            </div>
            <div class="col">
                <h2 class="fw-medium fz-medium white brush-before brush-green scrollreveal">Atelier Saint-Ambroise</h2>
                <p class="text white">73 boulevard Voltaire, 75011 Paris</p>
                <p class="text white"><a href="tel:0660757773">06 60 75 77 73</a></p>
            </div>
        </div>
    </div>
</footer>