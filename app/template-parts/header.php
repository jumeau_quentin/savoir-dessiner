<!doctype html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!-- Primary Meta Tags -->
    <title><?= $seoTitle; ?></title>
    <meta name="title" content="<?= $seoTitle; ?>">
    <meta name="description" content="<?= $seoDescription; ?>">

    <!-- Open Graph / Facebook -->
    <meta property="og:type" content="website">
    <meta property="og:url" content="<?php __site_uri(''); ?>/">
    <meta property="og:title" content="<?= $seoTitle; ?>" >
    <meta property="og:description" content="<?= $seoDescription; ?>">
    <meta property="og:image" content="<?php __img('photos/thumb.jpg'); ?>">

    <!-- Twitter -->
    <meta property="twitter:card" content="summary_large_image">
    <meta property="twitter:url" content="<?php __site_uri(''); ?>">
    <meta property="twitter:title" content="<?= $seoTitle; ?>">
    <meta property="twitter:description" content="<?= $seoDescription; ?>">
    <meta property="twitter:image" content="<?php __img('photos/thumb.jpg'); ?>">

    <link rel="icon" type="image/png" href="<?php __img('icon/play-yellow.svg'); ?>" />
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Jost:wght@400;500;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="<?php __css('style.css'); ?>">

    <!-- Google Tag Manager -->

    <!-- End Google Tag Manager -->
</head>