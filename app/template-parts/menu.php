<header class="main-menu bg-white">
    <div class="container">
        <div class="row">
            <div class="logo va-center">
                <img src="<?php __img('logo/logo.svg'); ?>" alt="Savoir dessiner" width="120">
            </div>
            <nav class="main-nav va-center">
                <ul>
                    <li><a href="">L'atelier online</a></li>
                    <li><a href="">Les RDV coaching</a></li>
                    <li><a href="">La box</a></li>
                    <li class="droplist"><a href="">À propos</a>
                        <ul style="display: none;">
                            <li><a href="">Subitem</a></li>
                            <li><a href="">Subitem</a></li>
                            <li><a href="">Subitem</a></li>
                        </ul>
                    </li>
                </ul>
            </nav>
            <nav class="seconday-nav va-center">
                <ul>
                    <li class="disabled">Le shop</li>
                    <li class="basket">Mon panier</li>
                    <li class="connect"><strong>Se connecter</strong></li>
                    <li>
                        <button class="cta-yellow">Commencer</button>
                    </li>
                </ul>
            </nav>
            <div class="burger">
                <button>
                    <span></span>
                    <span></span>
                    <span></span>
                </button>
            </div>
        </div>
    </div>
</header>
