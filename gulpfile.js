const browsersync      = require("browser-sync"),
      gulp             = require("gulp"),
      phpConnect       = require('gulp-connect-php'),
      clean            = require('gulp-clean'),
      rename           = require('gulp-rename'),

      sass             = require('gulp-sass'),
      sourcemaps       = require('gulp-sourcemaps'),
      autoprefixer     = require('gulp-autoprefixer'),

      cleanCSS         = require('gulp-clean-css'),
      htmlmin          = require('gulp-htmlmin'),

      babel            = require('gulp-babel'),
      plumber          = require('gulp-plumber'),
      uglify           = require('gulp-uglify'),
      stripComments    = require('gulp-strip-comments'),
      stripConsole     = require('gulp-strip-debug'),

      useref           = require('gulp-useref'),

      cache            = require('gulp-cache'),
      imagemin         = require('gulp-imagemin'),
      imageminPngquant = require('imagemin-pngquant'),
      imageminZopfli   = require('imagemin-zopfli'),
      imageminMozjpeg  = require('imagemin-mozjpeg'),
      imageminGiflossy = require('imagemin-giflossy');


// Setup Php Virtual Server
function connectsync() {
    phpConnect.server({
        port: 3000,
        keepalive: true,
        base: "dist"
    }, function () {
        browsersync({
            proxy: 'localhost:3000'
        });
    });
}

function browserSyncReload(done) {
    browsersync.reload();
    done();
}

// general
function cleanAll() {
    return gulp.src('dist/**/*')
        .pipe(clean())
}

// Move all assets to /dist
function devMove() {
    return gulp.src([
        'app/**/*.html',
        'app/**/*.php',
        'app/assets/js/**/*.js',
        'app/assets/fonts/**/*',
        'app/assets/sound/**/*',
        'app/assets/src/**/*'], {since: gulp.lastRun(devMove), base: 'app'})
        .pipe(gulp.dest("./dist"));
}

// SASS + css.map
function devSass() {
    return gulp.src('app/assets/scss/**/*.scss')
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(sass({outputStyle: ''})
            .on('error', sass.logError))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(sourcemaps.write('.'))

        .pipe(gulp.dest('dist/assets/css'))
    // .pipe(browserSync.reload({stream: true}))
}

function devJs() {
    return gulp.src('app/assets/js/**/*.js')
        .pipe(sourcemaps.init())
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/assets/js'))
}

// minify Img
function images() {
    return gulp.src('app/assets/img/**/*', {since: gulp.lastRun(images)})
        .pipe(cache(imagemin([
            //png
            imageminPngquant({
                speed: 1,
                quality: [0.95, 1] //lossy settings
            }),
            imageminZopfli({
                more: true
                // iterations: 50 // very slow but more effective
            }),
            //gif
            // imagemin.gifsicle({
            //     interlaced: true,
            //     optimizationLevel: 3
            // }),
            //gif very light lossy, use only one of gifsicle or Giflossy
            imageminGiflossy({
                optimizationLevel: 3,
                optimize: 3, //keep-empty: Preserve empty transparent frames
                lossy: 2
            }),
            //svg
            imagemin.svgo({
                plugins: [{
                    removeViewBox: false

                }]
            }),
            //jpg lossless
            /*            imagemin.jpegtran({
                            progressive: true
                        }),*/
            //jpg very light lossy, use vs jpegtran
            imageminMozjpeg({
                quality: 90
            })
        ])))
        .pipe(gulp.dest('dist/assets/img'))
    /*.pipe(browserSync.reload({stream: true}))*/
}


function watchFiles() {
    gulp.watch("app/**/*.php", gulp.series(devMove, browserSyncReload));
    gulp.watch("app/assets/js/**/*.js", gulp.series(devMove, browserSyncReload));
    gulp.watch("app/assets/fonts/**/*", gulp.series(devMove, browserSyncReload));
    gulp.watch("app/**/*.html", gulp.series(devMove, browserSyncReload));
    gulp.watch("app/assets/src/**/*", gulp.series(devMove, browserSyncReload));
    gulp.watch("app/assets/scss/**/*.scss", gulp.series(devSass, browserSyncReload));
    gulp.watch("app/assets/img/**/*", gulp.series(images, browserSyncReload));
    gulp.watch("app/assets/sound/**/*", gulp.series(devMove, browserSyncReload));

}

// Dist Prod env

// Clean for prod
function cleanProd() {
    return gulp.src([
        'dist/**/*.html',
        'dist/**/*.php',
        'dist/assets/js/**/*.js',
        'dist/assets/css/**/*'
    ])
        .pipe(clean())
}


// move html/php
function prodMove() {
    return gulp.src([
        'app/**/*.html',
        'app/**/*.php',
        'app/assets/js/**/*.js',
        'app/assets/fonts/**/*',
        'app/assets/src/**/*',
        'app/assets/sound/**/*',
    ], {base: 'app'})
        .pipe(gulp.dest('./dist'))

}


// Build sass + clean-css + minify css
function prodSass() {
    return gulp.src('app/assets/scss/**/*.scss')
        .pipe(sass())
        .pipe(sass({outputStyle: ''})
            .on('error', sass.logError))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(gulp.dest('dist/assets/css'))
}


function cssMinify() {
    return gulp.src('dist/assets/css/*.css')
        .pipe(cleanCSS())
        .pipe(autoprefixer({
            cascade: false
        }))
        //.pipe(rename('style.min.css'))
        .pipe(gulp.dest('dist/assets/css'))
}


// Concat js + uglify
function jsCompile() {
    return gulp.src(['node_modules/babel-polyfill/dist/polyfill.js','app/assets/js/**/*.js'])
        .pipe(plumber())
        // Transpile the JS code using Babel's preset-env.
        .pipe(babel({
            presets: [
                ['@babel/env', {
                    modules: false,
                    targets: [
                        'last 2 versions',
                        'not dead',
                        'ie 11',
                    ],
                }]
            ]
        }))
        // Save each component as a separate file in dist.
        .pipe(gulp.dest('dist/assets/js'))
}

function jsMinify() {
    return gulp.src('dist/assets/js/**/*.js')
        .pipe(uglify())
        .pipe(stripComments())
        .pipe(stripConsole())
        //.pipe(rename({suffix: ".min"}))
        .pipe(gulp.dest('dist/assets/js'))
}

function cleanJsMin() {
    return gulp.src('dist/assets/js/**/*.min.min.js')
        .pipe(clean())
}


// Useref
function useRef() {
    return gulp.src([
        'dist/**/*.html',
        'dist/**/*.php'
    ])
        .pipe(useref({
            transformPath: function (filePath) {
                return filePath.replace('/template-parts', '')
            }
        }))
        .pipe(gulp.dest('dist'));
}


// html/php min
function htmlMinify() {
    return gulp.src([
        'dist/**/*.html',
        'dist/**/*.php',
        '!dist/config/**/*.php',
        '!dist/controller/**/*.php',
    ])
        .pipe(htmlmin({
            collapseWhitespace: true,
            removeComments: true,
            includeAutoGeneratedTags: false,
            ignoreCustomFragments: [/<%[\s\S]*?%>/, /<\?[=|php]?[\s\S]*?\?>/]
        }))
        .pipe(gulp.dest('./dist'))
}


// Single Dev commands
exports.cleanAll = cleanAll;
exports.devMove = devMove;
exports.sass = devSass;
exports.devJs = devJs;
exports.images = images;

// Single Prod commands
exports.clean = cleanProd;
exports.prodMove = prodMove;
exports.prodSass = prodSass;
exports.cssMinify = cssMinify;
exports.jsCompile = jsCompile;
exports.jsMinify = jsMinify;
exports.cleanJs = cleanJsMin;
exports.useRef = useRef;
exports.htmlMinify = htmlMinify;

const watch = gulp.parallel([devMove, devSass, devJs, images, watchFiles, connectsync]);
exports.dev = watch;
exports.production = gulp.series(cleanProd, gulp.parallel(prodMove, images), prodSass, gulp.parallel(cssMinify, jsCompile), jsMinify, useRef, htmlMinify);

// ENV
exports.default = process.env.NODE_ENV === 'production' ? exports.production : exports.dev;